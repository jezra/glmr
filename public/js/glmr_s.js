UI = 'simple';

$(function(){
		//implement the jquery UI
		$("#modes").buttonset();
		$("button").button();
		$( "#slider" ).slider({
      min: 0,
      max: 255,
      value: 0,
      orientation: "vertical",
      slide: slider_change
		});

    //set up the buttons
    $("#full").click( clicked_full );
    $("#off").click( clicked_off );

    //get the server status
    get_status();
    //check presets
    check_presets();
});

function slider_change(event, slider){
	var h = to_hex( slider.value );
	glmr_set_color(h+h+h);
}

function color_changed() {
	var h = to_hex( $("#slider").slider("value") );
	glmr_set_color(h+h+h);
}

function get_status() {
	var url = "/status";
  $.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : color : "+type);
    }
  });
}

function clicked_full() {
	$( "#slider" ).slider("value", 255);
	color_changed();
}

function clicked_off() {
	$( "#slider" ).slider("value", 0);
	color_changed();
}

