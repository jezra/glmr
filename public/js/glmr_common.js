function to_hex(number) {
	num = parseInt(number,10);
	if (num==0) {
		return "00";
	}
	num = Math.max(0,Math.min(num,255));
	h1 = "0123456789ABCDEF".charAt((num-num%16)/16);
	h2 ="0123456789ABCDEF".charAt(num%16);
	return h1+h2;
}

function glmr_set_color( color ){
	var url = "/color/"+color;
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error : color : "+type);
		}
	});
}

function check_presets() {
	var url = "/presets";
	$.ajax({
		url: url,
		dataType: 'text',
		success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			process_presets(body);
		},
		error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
			log("error : color : "+type);
		}
	});
}

function process_presets(data) {
	var presets = $.parseJSON(data);
	if (presets.length > 0) {
		//show the presets div
		$("#presets").show();
		$.each(presets, function(index, preset){
			var btn = $("<button>");
			btn.text( preset );
			$("#presets").append(btn);
			btn.css("border", "5px solid #"+preset);
			btn.button();
			btn.click({index:index,preset:preset}, preset_click );
		});
	}
}

function preset_click(event) {

	/* are we in the simple UI? */
	if (UI == 'simple') {
		var index = event.data.index;
		var url = "/preset/"+index;
		$.ajax({
			url: url,
			dataType: 'text',
			success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
				//pass
			},
			error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
				log("error : preset : "+type);
			}
		});
	} else { //this is the advanced UI
			/* we just want the set the color selector */
		//get the preset
		var preset = event.data.preset;
		//update the color picker
		set_color(preset);
	}
}

