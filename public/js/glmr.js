//some global variables
colors = new Array();
colors["red"] = 0;
colors["green"] = 0;
colors["blue"] = 0;
current_color = "000000";

UI = 'advanced';

$(function(){
		//implement the jquery UI
		$("#modes").buttonset();
		$("button").button();
		$( ".rgb_slider" ).slider({
      min: 0,
      max: 255,
      value: 0,
      slide: slider_change
		});

    //set up the buttons
    $("#set").click( clicked_set );
    $("#fade_to").click( clicked_fade_to );
    //mode buttons

    $("#chase").click( clicked_chase );
    $("#full").click( clicked_full );
    $("#twinkle").click( clicked_twinkle );
    //get the server status
    get_status();
    //check presets
    check_presets();
});

function slider_change(event, slider){
	colors[$(slider.handle.parentElement).attr("id")] = slider.value;
	update_color_picker();
}

function update_color_picker() {
	current_color = to_hex( colors['red']) +to_hex( colors['green']) +to_hex( colors['blue'])
	$("#color_picker").val(current_color);
	$("#color_picker_wrapper").css("background-color", "#"+current_color);
}

function get_picker_color() {
	return $("#color_picker").val();
}

function set_color( color ) {
	var r = color.substr(0,2);
	var g = color.substr(2,2);
	var b = color.substr(4,2);
	colors["red"] = parseInt(r,16);
	colors["green"] = parseInt(g,16);
	colors["blue"] = parseInt(b,16);
	$("#red").slider("value", colors["red"]);
	$("#green").slider("value", colors["green"]);
	$("#blue").slider("value", colors["blue"]);
	//
	update_color_picker();
}

function set_mode( mode ) {
	var b = $("#"+mode);
	b.attr('checked',true);
	b.button("refresh");
}

function get_status() {
	var url = "/status";
  $.ajax({
    url: url,
    dataType: 'json',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)
			set_color( body['color'] );
			set_mode( body['mode'] );
    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : color : "+type);
    }
  });
}

function clicked_set() {
	glmr_set_color( get_picker_color() );
}

function clicked_fade_to() {
	var url = "/fade/"+get_picker_color();
  $.ajax({
    url: url,
    dataType: 'text',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : fade : "+type);
    }
  });
}

function clicked_chase() {
	var url = "/mode/chase";
  $.ajax({
    url: url,
    dataType: 'text',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : chase : "+type);
    }
  });
}

function clicked_full() {
	var url = "/mode/full";
  $.ajax({
    url: url,
    dataType: 'text',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : full : "+type);
    }
  });
}

function clicked_twinkle() {
	var url = "/mode/twinkle";
  $.ajax({
    url: url,
    dataType: 'text',
    success: function(body) { // body is a string (or if dataType is 'json', a parsed JSON object)

    },
    error: function(xhr, type) { // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
      log("error : twinkle : "+type);
    }
  });
}

function log( string ) {
	try {
		console.log(string);
	} catch (e) {
		alert( string);
	}

}
