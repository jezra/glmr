GLMR
===

GLMR is the GLMR Light Manipulation R.. something or other.

####Usage:

1. copy config.rb.tmp to config.rb
2. edit config.rb to match your system setup
3. copy presets.rb.tmp to presets.rb
4. edit presets.rb to contain your desired preset colors
5. run glmr.rb

####Config Parameters:

PORT : The port for the server to use  
SPIDEV :  the path of the SPI device to use  
NUMPIXELS : how many LEDs are on the LED string

