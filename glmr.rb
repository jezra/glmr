#!/usr/bin/env ruby

# -- This code is licensed GPLv3 --
# -- https://www.gnu.org/licenses/gpl.html

presets = File.dirname(__FILE__) +'/presets.rb'
load presets if File.exists? presets
load File.dirname(__FILE__) +'/config.rb'
#define OFF
OFF="000000"

#make an enum type thing for the various Modes
Mode = [TWINKLE='twinkle', FULL='full', CHASE='chase']

class WS2801
	def initialize(device, len)
		@spidev = File.open(device,"wb") unless @@TESTING
		@num_pixels = len
		@pixels = Array.new(@num_pixels)
		@chase_sleep = 0.1
		@fade_time = 1
		@twinkle_sleep = 0.1
		@fading = false
		@mode = FULL
		@previous_mode = FULL
		@previous_color = "000000"
		@current_color = "000000"
		@current_preset = -1
	end

	def all_pixels( color )
		#convert the string to a binary hex
		color_hex =  hexpack(color)
		#set all of the pixels to a specific color
		(0..@num_pixels-1).each do |i|
			@pixels[i] = color_hex
		end
		blit()
	end

	def current_color(color=nil)
		unless color.nil?
			if color == "previous"
				color = @previous_color
			end
			@previous_color = @current_color
			@current_color = color
			@current_color_hex = [color].pack("H*")
			color_changed()
		end
		@current_color
	end

	def process_preset(preset=nil, fade=nil)
		if PRESET.nil? or PRESET.length == 0
			return "No Presets Exists"
		end
		case preset
			when "previous"
				@current_preset-=1
				@current_preset = PRESET.length()-1 if @current_preset < 0

			when "next"
				@current_preset+=1
				if @current_preset > PRESET.length()-1
					@current_preset = 0
				end
			when "0".."#{PRESET.length()-1}"
				@current_preset = preset.to_i
			else
				return "error: preset out of range"
		end
		if preset.empty? or preset=="/preset"
			return PRESET.inspect
		else
			color = PRESET[@current_preset]
			if fade
				fade_color(color)
			else
				current_color(color)
			end
			return "#{@current_preset} : #{@current_color}"
		end
	end

	def color_changed()
		case @mode
			when FULL
				all_pixels( @current_color )
		end
	end

	def blit()
		unless @@TESTING
			@pixels.each do |pixel|
				@spidev.write(pixel)
			end
			@spidev.flush()
		else

		end
	end

	def mode(mode=nil)
		return @mode unless mode
		if Mode.include? mode
			#keep track of the mode
			@previous_mode = @mode
			@mode = mode
			case @mode
				when "full"
					kill_thread
					all_pixels( @current_color )
				when "twinkle"
					kill_thread
					@thread = Thread.new do
						do_twinkle
					end
				when "chase"
					kill_thread
					@thread = Thread.new do
						do_chase
					end
			end
			return @mode
		elsif mode == "previous"
			mode(@previous_mode)
		else
			return "invalid mode: #{mode}"
		end
	end

	def kill_thread
    Thread.kill(@thread) if !@thread.nil?
  end

  def do_chase
		off_hex = hexpack(OFF)
		all_pixels(OFF)
		target = -1
		prev_target = 0
		direction = "i"
		while @mode == CHASE
			prev_target = target
			if direction == "i"
				target += 1
				if target == @num_pixels-1
					direction = "d"
				end
			else
				target -= 1
				if target == 0
					direction = "i"
				end
			end
			@pixels[prev_target]=off_hex
			@pixels[target]=@current_color_hex
			blit()
			sleep @chase_sleep
		end
  end

  def do_twinkle
		off_hex = hexpack(OFF)
		all_pixels(OFF)
		while @mode == TWINKLE
			(0..@num_pixels-1).each do |i|
				color = [off_hex, @current_color_hex].sample()
				@pixels[i] = color
			end
			blit()
			sleep @twinkle_sleep
		end
  end

	def fade_color( hex_string, speed = @fade_time )
		return "FADING" if @fading
		@fading = true
		#do this in a thread
		Thread.new do
			#how many steps should there be?
			steps = 100
			fade_sleep = speed.to_f/steps
			#convert the hex strings to RGB ints
			hr = hex_string[0,2].to_i(16)
			hg = hex_string[2,2].to_i(16)
			hb = hex_string[4,2].to_i(16)
			#get RGB int values for the current_color
			cr = @current_color[0,2].to_i(16)
			cg = @current_color[2,2].to_i(16)
			cb = @current_color[4,2].to_i(16)

			#get the difference in steps for each color
			rs = (0.0+hr - cr)/steps
			gs = (0.0+hg - cg)/steps
			bs = (0.0+hb - cb)/steps

			#loop the number of steps and update the current color
			(1..steps).each do
				cr += rs
				cg += gs
				cb += bs
				new_color = "%02x%02x%02x" % [cr.to_i, cg.to_i, cb.to_i]
				current_color(new_color)
				sleep fade_sleep
			end
			#set the final color, just to be sure
			new_color = "%02x%02x%02x" % [hr, hg, hb]
			current_color(new_color)
			#we are no longer fading
			@fading = false
		end
		return "FADE STARTED"
	end

	def hexpack ( hex )
		[hex].pack("H*")
	end

end

if $0 == __FILE__
	require 'webrick'
	require 'json'

	#set some variables about the ws2801
	dev = "/dev/spidev0.0"
	num_pixels = 50

	#are we testing?
	@@TESTING = ARGV[0].nil? ? false : true

	#create a ws2801
	ws2801 = WS2801.new(SPIDEV,NUMPIXELS)

	#define some server information
	server_public_dir = File.join(
		File.expand_path( File.dirname(__FILE__) ),
		"public"
	)

	#create the server
	server = WEBrick::HTTPServer.new({:Port=>PORT, :DocumentRoot=>server_public_dir})
	#what urls do we need to mount?

	server.mount_proc('/color') do |req, res|
		color_string = req.path.sub("/color/", "")
		new_color = ws2801.current_color(color_string)
		res['Content-Type'] = 'text/plain'
		res.body = new_color
	end

	server.mount_proc('/fade') do |req, res|
		info = req.path.sub("/fade/", "").split("/")
		color = info[0]
		if (info[1])
			speed = info[1]
			new_color=ws2801.fade_color(color, speed)
		else
			new_color=ws2801.fade_color(color)
		end
		res['Content-Type'] = 'text/plain'
		res.body = new_color
	end

	server.mount_proc('/status') do |req, res|
		color = ws2801.current_color()
		mode = ws2801.mode()
		res['Content-Type'] = 'text/plain'
		res.body = JSON.dump( {:color=>"#{color}", :mode=>"#{mode}"} )
	end

	server.mount_proc('/mode') do |req, res|
		mode = req.path.sub("/mode/", "")
		return_val = ws2801.mode(mode)
		res['Content-Type'] = 'text/plain'
		res.body = return_val
	end

	server.mount_proc('/preset') do |req, res|
		preset = req.path.sub("/preset/", "")
		return_val = ws2801.process_preset(preset)
		res['Content-Type'] = 'text/plain'
		res.body = return_val
	end

	server.mount_proc('/presets') do |req, res|
		return_val = PRESET.inspect
		res['Content-Type'] = 'text/plain'
		res.body = return_val
	end

	server.mount_proc('/fade/preset') do |req, res|
		preset = req.path.sub("/fade/preset/", "")
		return_val = ws2801.process_preset(preset,true)
		res['Content-Type'] = 'text/plain'
		res.body = return_val
	end

  #when CTRL+c is pressed, we want to shutdown
	trap "INT" do
		server.shutdown
	end

	#start the server
	server.start

end

